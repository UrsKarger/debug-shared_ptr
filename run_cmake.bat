@echo off

call .\scripts\cmake_cfg.bat

REM set OPT_GENERATOR="Visual Studio 16 2019"
set OPT_GENERATOR="Ninja"
REM set OPT_GENERATOR="Ninja Multi-Config"

if NOT EXIST "%OPT_PROJECT_WS_DIR%" (
    echo. "creating workspace directory %OPT_PROJECT_WS_DIR%"
    mkdir "%OPT_PROJECT_WS_DIR%"
)
REM else (
REM    del "%OPT_PROJECT_WS_DIR%\CMakeCache.txt"
REM )

choice /C YN /T 3 /D N /M "change project name (default:%OPT_PROJECT_NAME%)"
if ERRORLEVEL 255 goto :run_cmake_cmd
if ERRORLEVEL 2 goto :run_cmake_cmd
if ERRORLEVEL 1 goto :change_project_name
if ERRORLEVEL 0 goto :run_cmake_cmd

:change_project_name
set /p MY_PROJECT_NAME_INP="Project Name [Default %OPT_PROJECT_NAME%] :"
if NOT "X%MY_PROJECT_NAME_INP%" == "X" (
    set OPT_PROJECT_NAME=%MY_PROJECT_NAME_INP%
)

:run_cmake_cmd

pushd %OPT_PROJECT_WS_DIR%

set PRJ_ROOT=%~dp0
echo. project name: %OPT_PROJECT_NAME%
echo. project root: %PRJ_ROOT%

%CMAKE_CMD% ^
    -G %OPT_GENERATOR% ^
    -DOPT_PROJECT_NAME=%OPT_PROJECT_NAME% ^
    -DOPT_CMAKE_INCL_PATH=%OPT_CMAKE_INCL_PATH% ^
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ^
    -Wdeprecated ^
    %OPT_PROJECT_ROOT_DIR%

echo. 
echo. "to start from scratch again"
echo. "rmdir /S %OPT_PROJECT_WS_DIR% && .\run_cmake.bat"
echo. 

popd

goto :EOF

:get_absolute
set %~2=%~f1
exit /b 0

:EOF
REM pause
