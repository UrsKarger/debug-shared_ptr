@echo off

call .\scripts\cmake_cfg.bat

REM set PRJ_ROOT=%~dp0
REM echo project root: %PRJ_ROOT%

REM >>> set the build config

echo "starting %BUILD_CFG% build in %OPT_PROJECT_WS_DIR%"
%CMAKE_CMD% ^
    --build %OPT_PROJECT_WS_DIR% ^
    --target %OPT_NAME_OF_EXE% ^
    --config %OPT_PROJECT_BUILD_CFG%
    
REM echo "starting release build in %OPT_PROJECT_WS_DIR%"
REM %CMAKE_CMD% ^
REM     --build %OPT_PROJECT_WS_DIR% ^
REM     --target %OPT_NAME_OF_EXE% ^
REM     --config Release
    