@echo off

set OPT_PROJECT_NAME=dbg_shared_ptr
set OPT_PROJECT_ROOT_DIR=.
set OPT_PROJECT_WS_DIR=build
set OPT_PROJECT_TEST_SRC_DIR=test\src

set OPT_ADD_CMAKE_INCL_PATH=cmake_ext
set CMAKE_PATH=c:\Program Files\CMake

set CMAKE_CMD="%CMAKE_PATH%\bin\cmake.exe"
set CMAKE_TEST_CMD="%CMAKE_PATH%\bin\ctest.exe"


set OPT_PROJECT_TEST_WS_DIR="%OPT_PROJECT_WS_DIR%\%OPT_PROJECT_TEST_SRC_DIR%"
call :get_absolute %OPT_ADD_CMAKE_INCL_PATH% OPT_CMAKE_INCL_PATH
call :get_absolute %OPT_PROJECT_ROOT_DIR% OPT_PROJECT_ROOT_DIR
call :get_absolute %OPT_PROJECT_WS_DIR% OPT_PROJECT_WS_DIR
call :get_absolute %OPT_PROJECT_TEST_WS_DIR% OPT_PROJECT_TEST_WS_DIR
call :get_absolute %OPT_PROJECT_TEST_SRC_DIR% OPT_PROJECT_TEST_SRC_DIR
call :test_cmake_cmd

:test_cmake_cmd
%CMAKE_CMD% --help > NUL 2>&1
if ERRORLEVEL 9009 (
    echo !! ERROR !!
    echo CMAKE not found - looked here %CMAKE_CMD%
    echo !! ERROR !!
    goto EOF
)

goto :EOF

:get_absolute
set %~2=%~f1
exit /b 0

:EOF
REM pause
