@echo off

call .\scripts\cmake_cfg.bat

REM set MY_TEST_MODULE=data_api
set MY_TEST_MODULE=file_io
set MY_TEST_SELECTION=test_all.txt
set MY_TESTREPORT_DIR_NAME=test_reports


set MY_TEST_SELECTION_FILE="%OPT_PROJECT_TEST_SRC_DIR%\%MY_TEST_MODULE%\%MY_TEST_SELECTION%"
set MY_TEST_RESULTS="%OPT_PROJECT_ROOT_DIR%\%MY_TESTREPORT_DIR_NAME%\test_results.xml"
set MY_TEST_EXE="%OPT_PROJECT_TEST_WS_DIR%\%MY_TEST_MODULE%\%OPT_PROJECT_BUILD_CFG%\ut_%MY_TEST_MODULE%.exe"

echo "configuration complete"
goto :remove_old_results

:remove_old_results
IF EXIST "%OPT_PROJECT_ROOT_DIR%\%MY_TESTREPORT_DIR_NAME%" (
    echo "removing old reports"
    rmdir "%OPT_PROJECT_ROOT_DIR%\%MY_TESTREPORT_DIR_NAME%" /q /s
)
echo "creating test report directory"
mkdir "%OPT_PROJECT_ROOT_DIR%\%MY_TESTREPORT_DIR_NAME%"
copy NUL > %MY_TEST_RESULTS%




:run_tests
echo ::: 
echo : running Tests of:
echo : %MY_TEST_EXE%
echo :::
REM call %MY_TEST_EXE% -r junit --out %MY_TEST_RESULTS% --input-file %MY_TEST_SELECTION_FILE%
call %MY_TEST_EXE% -r console --input-file %MY_TEST_SELECTION_FILE%

:EOF
REM pause