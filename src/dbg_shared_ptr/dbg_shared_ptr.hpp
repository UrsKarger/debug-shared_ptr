#ifndef DBG_SHARED_PTR_HPP_
#define DBG_SHARED_PTR_HPP_

/** \file dbg_shared_ptr.hpp */

#include <cstdint>
#include <atomic>

#define DEBUG_SHARED_PTR_NAMESPACE debug

namespace DEBUG_SHARED_PTR_NAMESPACE
{

template <typename T> class shared_ptr;
template <typename T> class weak_ptr;

struct _RefCounterBase
{
   using Value_t = std::uint32_t;

   std::atomic<Value_t> _uses;
   std::atomic<Value_t> _weaks;

   _RefCounterBase( ) noexcept
      : _uses(1)
      , _weaks(1)
   {
   }

   _RefCounterBase(const _RefCounterBase &) = delete;
   _RefCounterBase &operator=(_RefCounterBase &rhs) = delete;
   virtual ~_RefCounterBase( ) = default;
   _RefCounterBase(_RefCounterBase &&other) noexcept
      : _uses(other._uses.load(std::memory_order_relaxed))
      , _weaks(other._weaks.load(std::memory_order_relaxed))
   {
   }

   _RefCounterBase &operator=(_RefCounterBase &&rhs) noexcept
   {
      _uses.store(rhs._uses.load(std::memory_order_relaxed), std::memory_order_relaxed);
      _weaks.store(rhs._weaks.load(std::memory_order_relaxed), std::memory_order_relaxed);

      return *this;
   }

   /** destroy managed resource */
   virtual void _Destroy( ) noexcept = 0;
   /** self destruct */
   virtual void _Delete_this( ) noexcept = 0;


   Value_t use_count( ) const { return _uses.load(std::memory_order_relaxed); }
   Value_t weak_count( ) const { return _uses.load(std::memory_order_relaxed); }

   void increment_ref( ) { (void)_uses.fetch_add(1, std::memory_order_relaxed); }

   /** decrement the ref count *
    *
    * \return value before the decrement
    */
   Value_t decrement_ref( ) { return _uses.fetch_sub(1, std::memory_order_relaxed); }
   bool increment_ref_when_not_zero( )
   {
      bool success = false;
      Value_t count = _uses.load(std::memory_order_relaxed);
      while (!success && count != 0)
      {
         Value_t new_value{count + 1};
         success =
            _uses.compare_exchange_weak(count, new_value, std::memory_order_relaxed);
      }

      return success;
   }

   void increment_weak( ) { (void)_weaks.fetch_add(1, std::memory_order_relaxed); }
   Value_t decrement_weak( ) { return _weaks.fetch_sub(1, std::memory_order_relaxed); }
};

template <typename T> class _RefCounterResource : public _RefCounterBase
{
 public:
   _RefCounterResource(T *t)
      : _ptr(t)
   {
   }

   /** destroy managed resource */
   void _Destroy( ) noexcept override
   {
      delete _ptr;
      _ptr = nullptr;
   }

   /** self destruct */
   void _Delete_this( ) noexcept override { _Destroy( ); }

 private:
   T *_ptr;
};


template <typename T, typename D> class _RefCounterWithDeleter : public _RefCounterBase
{
 public:
   _RefCounterWithDeleter(T *t, D deleter)
      : _ptr(t)
      , _deleter(std::move(deleter))
   {
   }

   /** destroy managed resource */
   void _Destroy( ) noexcept override
   {
      _deleter(_ptr);
      _ptr = nullptr;
   }

   /** self destruct */
   void _Delete_this( ) noexcept override { delete this; }

 private:
   T *_ptr;
   D _deleter;
};

template <typename T> class _PtrBase
{
 public:
   using element_t = std::remove_all_extents_t<T>;

 private:
   template <typename U> friend class shared_ptr;

   template <typename V> friend class weak_ptr;
   template <typename W> friend class _PtrBase;

   _RefCounterBase *_ref_count;
   T *_ptr;

 protected:
   _PtrBase( ) noexcept
      : _ref_count(nullptr)
      , _ptr(nullptr)
   {
   }

   _PtrBase(T *t)
      : _ref_count{nullptr}
      , _ptr(t)
   {
      if (_ptr)
      {
         _ref_count = new _RefCounterResource(_ptr);
      }
   }

   _PtrBase(const shared_ptr<T> &shared)
      : _ref_count(shared._ref_count)
      , _ptr(shared._ptr)
   {
      _ref_count->increment_ref( );
   }

   _PtrBase(const weak_ptr<T> &weak)
   {
      if (!this->shared_from_weak(weak))
      {
         throw bad
      }
   }

   virtual ~_PtrBase( )
   {
      if (_ref_count)
      {
         auto ref_count = _ref_count->decrement_ref( );
         if (ref_count == 0)
         {
            //_deleter(_ptr);
            delete _ptr;
            _ptr = nullptr;
         }
         if (_ref_count->weak_count( ) == 0)
         {
            delete _ref_count;
            _ref_count = nullptr;
         }
      }
   }

   void _swap(_PtrBase &other) noexcept
   {
      using std::swap;
      swap(this->_ref_count, other._ref_count);
      swap(this->_ptr, other._ptr);
   }

   template <typename Tx> bool shared_from_weak(const weak_ptr<Tx> &other)
   {
      if (other._ref_count && other._ref_count->increment_ref_when_not_zero( ))
      {
         _ptr = other._ptr;
         _ref_count = other._ref_count;
         return true;
      }
      return false;
   }

   void release( )
   {
      if (_ref_count->decrement_ref( ) == 0)
      {
         _ref_count->_Destroy( );
         if (_ref_count->decrement_weak( ) == 0)
         {
            _ref_count->_Delete_this( );
         }
      }
   }

 public:
   _RefCounterBase::Value_t use_count( )
   {
      return _ref_count ? _ref_count->use_count( ) : 0;
   }
   [[nodiscard]] element_t *get( ) const noexcept { return _ptr; }
};

template <typename T> class shared_ptr : public _PtrBase<T>
{
 public:
   shared_ptr( ) noexcept
      : _PtrBase( )
   {
   }

   shared_ptr(T *t)
      : _PtrBase(t)
   {
   }

   shared_ptr (const shared_ptr &other) noexcept
   {
      _ptr = other._ptr;
      _ref_count = other._ref_count;
      _ref_count->increment_ref( );
   }

   shared_ptr& operator=(const shared_ptr &other)
   {
      // Guard self assignment
      if (this == &other)
         return *this;

      tmp = shared_ptr(other);
      this->swap(tmp);
      return *this;
   }

   shared_ptr(const weak_ptr<T> &weak)
   {
      if (!this->shared_from_weak(weak))
      {
         throw std::bad_weak_ptr;
      }
   }

   template <typename Tx, typename Dx>
   shared_ptr(Tx *t, Dx deleter)
      : _PtrBase(t)
   {
      _PtrBase::_ref_count = new _RefCounterWithDeleter(t, std::move(deleter));
   }

   void swap(shared_ptr &other) { this->_swap(other); }
   void reset( ) noexcept { shared_ptr( ).swap(*this); }
   template <typename _Tx> void reset(_Tx *t) { shared_ptr(t).swap(*this); }
   explicit operator bool( ) const noexcept { return get( ) != nullptr; }
};

template <typename T> class weak_ptr : public _PtrBase<T>
{
 public:
   weak_ptr<T>( ) noexcept
      : _PtrBase( ){ };

   weak_ptr<T>(shared_ptr<T> &shared)
      : _PtrBase(shared)
   {
      if (_ref_count)
      {
         _ref_count->increment_weak( );
      }
   }

   void swap(weak_ptr<T> &other) { _PtrBase::swap(other); }


   bool expired( ) { return use_count( ) == 0; }

   shared_ptr<T> lock( )
   {
      shared_ptr<T> ptr;
      (void)ptr.shared_from_weak(*this);
      return ptr;
   }
};

template <typename T, typename... Args> shared_ptr<T> make_shared(Args &&...args)
{
   return shared_ptr<T>(new T(args...));
}

template <typename T> void swap(shared_ptr<T> &left, shared_ptr<T> &right)
{
   left.swap(right);
}

template <typename T> void swap(weak_ptr<T> &left, weak_ptr<T> &right)
{
   left.swap(right);
}

}  // namespace DEBUG_SHARED_PTR_NAMESPACE


// Copyright Urs Karger, 2021. All rights reserved.
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#endif /* DBG_SHARED_PTR_HPP_ */
