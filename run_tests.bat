@echo off

call .\scripts\cmake_cfg.bat

REM set PRJ_ROOT=%~dp0
REM echo project root: %PRJ_ROOT%

echo "starting tests in %OPT_PROJECT_WS_DIR%"
pushd "%OPT_PROJECT_WS_DIR%"

REM >>> build first in case it was not done before
echo "starting %BUILD_CFG% build in %OPT_PROJECT_WS_DIR%"
REM %CMAKE_CMD% ^
REM     --build %OPT_PROJECT_WS_DIR% ^
REM    --config %OPT_PROJECT_BUILD_CFG%

%CMAKE_CMD% ^
    --build %OPT_PROJECT_WS_DIR%


%CMAKE_TEST_CMD% ^
    --build-config %OPT_PROJECT_BUILD_CFG% ^
    -j2
    
popd
